package mw;

class NullExtensions {
  static public inline function ifNull<T>(v:Null<T>, default_:T):T {
    return v != null ? v : default_;
  }
}
