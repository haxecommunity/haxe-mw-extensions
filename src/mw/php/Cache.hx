#if !macro
typedef CacheEntry = {
  timestamp: Int,
  data:Dynamic
}

interface CacheInterface {
  function cached(key:String, calc: Void -> String, ?ttl: Int):Dynamic;
  function clear():Void;
}

class File_Cache implements CacheInterface {

  static public inline function timestamp(): Float {
  return
    #if php
      untyped __call__("mktime")
    #else
      Date.now().getTime()
    #end
    ;
  }

  function store(key:String, value:Dynamic){
     untyped __call__("file_put_contents", '${dir}/${key}', __call__('serialize', value));
  }
  function fetch(key:String):Dynamic {
    var file = '${dir}/${key}';
    if (untyped __call__('file_exists', file)){
      return untyped __call__('unserialize', __call__('file_get_contents', file));
    } else return null;
  }

  var dir:String;
  public function new(dir:String = "cache") {
    this.dir = dir;
  }

  public function clear() {
    #if php
    untyped __call__("array_map", "unlink", __call("glob", '${dir}/*'));
    #else
      #error TODO
    #end
  }

  public function cached(key:String, calc: Void -> String, ?ttl:Int):Dynamic {
    var r: CacheEntry = cast(fetch(key));
    var stamp = timestamp();
    if (r == null || r.timestamp + ttl < stamp){
      var r = {
        timestamp: stamp,
        data: calc()
      }
      if (store != null) store(key, r);
    }
    return r.data;
  }

}

#if php
class APC_Cache_In_Memory implements CacheInterface {
  // TODO
  public function new(prefix:String = "") {
  }

  public function cached(key:String, calc: Void -> String,  ?ttl: Int):Dynamic {
    var r = null;
    untyped __php__("
      $r = apc_fetch($this->prefix.$key, $b);
      if (!$b){
        $r = calc();
        apc_store($this->$prefix.$key, $x, $ttl);
      }
    ");
    return r;
  }

  public function clear() { untyped __call__("apc_clear"); }

}
#end
