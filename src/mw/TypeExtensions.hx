package mw;

import haxe.macro.Expr;
import haxe.macro.Context;
import haxe.macro.Type;

class TypeExtensions {

  static public function typeParams(t:Type) {
    switch (t) {
      case TInst(_, params): return params;
      case _:
    };
    throw "unexpeced";
    return null;
  }

  static public function typeParamAsString(t:Type):String {
    switch (t) {
      case TInst(ref, _): 
        switch (ref.get().kind){
          case KExpr(e): return mw.macro.ExprExtensions.as_string(e);
          case _:
        }
      case _:
    }
    throw "bad: type parameter cannot be interpreted as string";
    return null;
  }

}
