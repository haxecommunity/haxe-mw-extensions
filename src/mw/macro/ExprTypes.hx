package mw.macro;
import haxe.macro.Expr;
import haxe.macro.Type;
import haxe.macro.Context;

class ExprTypes {
  static public function structTypes(e:Expr):Array<ClassField> {
    return switch (Context.typeof(e)) {
      case TAnonymous(a): a.get().fields;
      case _: throw 'TAnonymous (structure) expected';
    }
  }
}
