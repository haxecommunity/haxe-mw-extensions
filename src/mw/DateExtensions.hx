package mw;

using Std;

class DateExtensions {

  static public var mysql_like = ~/([0-9]+)-([0-9]+)-([0-9]+)/;
  static public var german = ~/([0-9]+)\.([0-9]+)\.([0-9]+)/;
  static public var english_like = ~/([0-9]+)\/([0-9]+)\/([0-9]+)/;

  // allowed formats:
  // YYYY-mm-dd (mysql like)
  // dd.mm.YYYY (German format)
  // mm/dd/YYYY (English format?)
  static public function parseDate(s:String) {
    if (mysql_like.match(s)){
      return Date.fromString(s);
    }
    if (german.match(s)){
      return new Date(german.matched(3).parseInt(), german.matched(2).parseInt(), german.matched(1).parseInt(), 0,0,0);
    }
    if (english_like.match(s)){
      return new Date(german.matched(3).parseInt(), german.matched(1).parseInt(), german.matched(2).parseInt(), 0,0,0);
    }
    // didn't parse
    return null;
  }
}
