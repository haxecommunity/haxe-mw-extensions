package mw;

class MySQLTools {
  // s must be Date or String
  static public function quoteDate(s:Dynamic) {
    if (Std.is(s, String))
      s = DateExtensions.parseDate(s);
    return DateTools.format(s, "%Y-%m-%d");
  }
}
