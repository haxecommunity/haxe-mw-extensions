package mw;

class NumberExtensions {
  static public function digits(x:Float, digits:Int):String {
    var f = Math.pow(10, digits);
    return (Std.int(f * x) / f) + "";
  }
}
